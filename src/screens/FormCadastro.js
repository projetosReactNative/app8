import React, { Component } from 'react';
import { View, TextInput, Button, StyleSheet, KeyboardAvoidingView } from 'react-native';
import { connect } from 'react-redux';
import { 
            modificaEmail, 
            modificaSenha, 
            modificaNome, 
            cadastraUsuarios 
        } from '../actions/AutenticacaoActions';

class formCadastro extends Component {
    _cadastraUsuario() {
        const { nome, email, senha } = this.props;
        this.props.cadastraUsuarios({ nome, email, senha });
    }

    render() {
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.view}>
                <View style={styles.viewForm}>
                    <TextInput 
                        value={this.props.nome} 
                        style={styles.textInputForm} 
                        placeholder="Nome" 
                        placeholderTextColor='rgba(0,0,0,0.2)'
                        underlineColorAndroid="transparent" 
                        onChangeText={texto => this.props.modificaNome(texto)}
                    />
                    <TextInput
                        keyboardType={('email-address')} 
                        value={this.props.email} 
                        style={styles.textInputForm} 
                        placeholder="E-mail" 
                        placeholderTextColor='rgba(0,0,0,0.2)' 
                        underlineColorAndroid="transparent"
                        onChangeText={texto => this.props.modificaEmail(texto)}
                    />
                    <TextInput 
                        secureTextEntry 
                        value={this.props.senha} 
                        style={styles.textInputForm} 
                        placeholder="Senha" 
                        placeholderTextColor='rgba(0,0,0,0.2)' 
                        underlineColorAndroid="transparent"
                        onChangeText={texto => this.props.modificaSenha(texto)} 
                    />
                    <View style={styles.viewButton}>
                        <Button 
                        title="Cadastrar" 
                        color={color3} 
                        onPress={() => this._cadastraUsuario()} 
                        />
                    </View>
                </View>
               
                
            </KeyboardAvoidingView>
        );
    }
}

const mapStateToProps = state => ({
    email: state.AutenticacaoReducer.email,
    senha: state.AutenticacaoReducer.senha,
    nome: state.AutenticacaoReducer.nome,
});

const actions = { modificaEmail, modificaSenha, modificaNome, cadastraUsuarios };
export default connect(mapStateToProps, actions)(formCadastro);

const color1 = '#293846';
//const color2 = '#337AB7';
const color3 = '#1ab394';
//const color4 = '#32BEA6';
const color5 = '#f3f3f4';

const styles = StyleSheet.create({
    view: {
        flex: 1, 
        padding: 10,
        backgroundColor: color5
    },
    viewForm: {
        flex: 4,
        justifyContent: 'center'
    },
    viewButton: {
        paddingTop: 15,
        flex: 1
    },
    textInputForm: {
        marginBottom: 20,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: color3,
        backgroundColor: 'rgba(255,255,255,1)',
        paddingHorizontal: 10,
        fontSize: 20,
        height: 45,
        color: color1
    },
    textForm: {
        marginBottom: 20,
        fontSize: 15,
        color: color1,
        textAlign: 'center'
    }
});
