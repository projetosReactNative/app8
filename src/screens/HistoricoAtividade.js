import React, { Component } from 'react';
import { ScrollView, View, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import { color1, color3 } from '../components/Colors';
import ListaTarefas from '../components/ListaTarefas';

class HistoricoAtividade extends Component {
   constructor(props) {
        super(props);
        this.state = {
            historico: this.props.historicoAtividade,
        };
   }

   verificaHistorico() {
       if (this.state.historico.length > 0) {
           return (
            <View style={{ marginBottom: 50 }}>
                {this.state.historico.map((item) => (<ListaTarefas key={item.id} item={item} />))}
            </View>
           );
       } 
        return (
            <View >
               <Text style={{ textAlign: 'center', fontSize: 18, fontWeight: '700' }}> 
                   Sem históricos para essa atividade 
                   </Text>
            </View>
           );
   }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'transparent' }}>
                <View style={styles.header}>
                    <Text style={styles.title}>{this.props.title}</Text>
                    <Text style={styles.description}>{this.props.description}</Text>
                </View>
                <ScrollView>
                    {this.verificaHistorico()}
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = state => (
        {
            title: state.AtividadeReducer.title,
            description: state.AtividadeReducer.description,
            historicoAtividade: state.AtividadeReducer.historicoAtividade
        }
    );

export default connect(mapStateToProps, null)(HistoricoAtividade);

const styles = StyleSheet.create({
    header: {
        alignItems: 'center',
        backgroundColor: color3,
    },
    title: {
        color: color1,
        textAlign: 'center',
        fontSize: 20,
        fontWeight: '800',
        
    },
    description: {
        color: 'white',
        textAlign: 'center',
        fontSize: 18,
        fontWeight: '400',
        padding: 5
    }
});
