import React, { Component } from 'react';
import { 
        View, 
        Text, 
        TextInput, 
        Button, 
        StyleSheet, 
        TouchableHighlight,
        KeyboardAvoidingView,
        StatusBar,
        Keyboard
       } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

import { modificaEmail, 
         modificaSenha, 
         validaEmail 
       } from '../actions/AutenticacaoActions';

_scrollToInput = (reactNode) => {
    // Add a 'scroll' ref to your ScrollView
    this.refs.scroll.scrollToFocusedInput(reactNode);
  };


class formLogin extends Component {
    render() {
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.view}>
                <StatusBar barStyle="default" />
                <View style={styles.viewTitulo}>
                    <Text style={styles.textTitulo}>
                        Lorem
                    </Text>
                    <Text style={styles.textTitulo2}>
                        Lorem Ipsum é simplesmente uma simulação de texto da indústria  
                    </Text>
                </View>
                <View style={styles.viewForm}>
                    <View>
                        <View>
                            <TextInput
                                keyboardShouldPersistTaps
                                value={this.props.email} 
                                style={styles.textInputForm} 
                                placeholder="E-mail" 
                                placeholderTextColor='rgba(0,0,0,0.2)' 
                                keyboardType={('email-address')} 
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onSubmitEditing={() => this.passwordInput.focus()}
                                underlineColorAndroid="transparent" 
                                onChangeText={
                                        texto => {
                                                    this.props.modificaEmail(texto);
                                                    this.props.validaEmail(texto, this.props.senha);
                                                 }
                                }

                            />
                        </View >
                        <View>
                            <Text style={{ color: '#ff0000', fontSize: 18 }}>
                                {this.props.erroEmail}
                            </Text>
                        </View>
                        <View>
                            <TextInput 
                                value={this.props.senha} 
                                style={styles.textInputForm} 
                                placeholder="Senha" 
                                returnKeyType="go"
                                ref={(input) => { this.passwordInput = input; }}
                                placeholderTextColor='rgba(0,0,0,0.2)'
                                underlineColorAndroid="transparent" 
                                onSubmitEditing={
                                    () => { 
                                        this.props.validaEmail(this.props.email, this.props.senha); 
                                        Actions.linhaDoTempo(); 
                                        Keyboard.dismiss(); 
                                    }
                                }
                                secureTextEntry 
                                onChangeText={
                                    texto => { 
                                                this.props.modificaSenha(texto); 
                                                this.props.validaEmail(this.props.email, texto);
                                            }
                                } 
                            />
                        </View>
                        <View style={{ marginBottom: 20 }}>
                            <Button 
                                title="Acessar" 
                                color={color3}
                                disabled={this.props.buttonDisabled} 
                                onPress={() => {
                                    Keyboard.dismiss();
                                    this.props.validaEmail(this.props.email, this.props.senha);
                                    Actions.linhaDoTempo();
                                }} 
                            />
                        </View>
                    </View>
                    <View>
                        <TouchableHighlight
                            underlayColor="transparent"
                            onPress={() => Actions.formCadastro()}
                        >
                            <Text style={styles.textForm}> Esqueci minha senha!</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </KeyboardAvoidingView>
        );
    }       
}

const mapStateToProps = state => ({
    email: state.AutenticacaoReducer.email,
    senha: state.AutenticacaoReducer.senha,
    erroEmail: state.AutenticacaoReducer.erroEmail,
    buttonDisabled: state.AutenticacaoReducer.buttonDisabled
});

export default connect(mapStateToProps, 
    { modificaEmail, modificaSenha, validaEmail })(formLogin);


const color1 = '#293846';
//const color2 = '#337AB7';
const color3 = '#1ab394';
//const color4 = '#32BEA6';
const color5 = '#f3f3f4';

const styles = StyleSheet.create({
    view: {
        flex: 1, 
        padding: 10,
        backgroundColor: color5,
       
    },
    viewTitulo: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textTitulo: {
        fontSize: 80,
        fontWeight: '800',
        color: 'rgba(103,106,108,0.4)' 
    },
    textTitulo2: {
        fontSize: 18,
        color: '#2f4050',
        textAlign: 'center',
        width: '75%',
        opacity: 0.5
    },
    textInputForm: {
        marginBottom: 20,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: color3,
        backgroundColor: 'rgba(255,255,255,1)',
        paddingHorizontal: 10,
        fontSize: 20,
        height: 45,
        color: color1
    },
    textForm: {
        marginBottom: 20,
        fontSize: 15,
        color: color1,
        textAlign: 'center'
    }
});
