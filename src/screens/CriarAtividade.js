import React, { Component } from 'react';
import { Text, View, Platform, Picker, TextInput } from 'react-native';
import { connect } from 'react-redux';
import { Icon as RNIcon, Button } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { alteraTipoAtividade, alteraDescricaoAtividade } from '../actions/CriarAtividadeActions';

import ModalPicker from '../components/ModalPicker';

 
class CriarAtividade extends Component {
 constructor(props) {
     super(props);
     this.state = { offset: 0, open: true, value: '', display: 'none', color: 'rgba(0,0,0,0.6)', textInputHeight: 40 };
     this.tipos = this.props.items;
     this.props.alteraTipoAtividade(this.state.value);
 }

 defineModalPicker() {
     if (Platform.OS === 'ios') {
         return (<ModalPicker />);
     } 
        return (
            <View>
                <Text style={{ textAlign: 'center', display: this.state.display }}>
                      Selecione o tipo
                </Text>
                <Picker style={{ color: this.state.color, height: 45, paddingVertical: 10 }} onValueChange={(itemValue) => { if (itemValue === 'selecione') {} else { this.setState({ color: 'black' }); this.props.alteraTipoAtividade(itemValue); } }} selectedValue={this.props.tipoAtividade}>
                    <Picker enabled={false} label="Selecione o tipo" value='selecione' style={{ color: 'blue' }} />
                    {this.tipos.map((item) => (<Picker.Item selected key={item.id} label={item.label} value={item.value} />))}
                </Picker>
            </View>
        );
 }

 heightChange(event) {
    const height = event.nativeEvent.contentSize.height;
    if (height > 40) {
        this.setState({ textInputHeight: height });
    } else {
        this.setState({ textInputHeight: 40 });
    }
 }

  render() {
    return (
        <View style={{ paddingHorizontal: 4 }}>
            <View style={{ padding: 8 }}>
                <View style={{ justifyContent: 'center', marginTop: 10, borderColor: 'black', borderBottomWidth: 1, height: Platform.OS === 'ios' ? 40 : undefined }}>
                    {this.defineModalPicker()}
                </View>
                <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 10, borderColor: 'black', borderBottomWidth: 1 }}> 
                    <TextInput 
                    value={this.props.descricaoAtividade} 
                                onChangeText={
                                        texto => {
                                                   this.props.alteraDescricaoAtividade(texto);
                                                 }
                                } multiline returnKeyType='done' placeholder="Descrição da atividade" style={{ width: '100%', height: this.state.textInputHeight, fontSize: 16, paddingTop: 10, color: 'black' }} onContentSizeChange={this.heightChange.bind(this)} underlineColorAndroid='transparent'
                    />
                </View>
            </View>
        </View>
        
    );
  }
}

const mapStateToProps = state => (
    {
        items: state.CriarAtividadeReducer.items,
        tipoAtividade: state.CriarAtividadeReducer.tipoAtividade,
        descricaoAtividade: state.CriarAtividadeReducer.descricaoAtividade
    }
);

export default connect(mapStateToProps, { alteraTipoAtividade, alteraDescricaoAtividade })(CriarAtividade);

/*

 <View style={{ backgroundColor: '#1AB394', borderRadius: 4, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', marginTop: 10 }}> 
                    <View style={{ flex: 8, justifyContent: 'center', paddingLeft: 8 }}>
                        <Text style={{ fontSize: 16, color: 'white' }}> Anexo </Text>
                    </View>
                    <View >
                        <View >
                            <Button transparent onPress={() => { alert('selecionar arquivo'); }} >
                                <Icon name='paperclip' size={27} color='white' /> 
                            </Button>  
                        </View>
                    </View>
                    <View style={{ marginHorizontal: 5 }}>
                        <View >
                            <Button transparent onPress={() => { alert('usar camera'); }} >
                                <RNIcon ios='ios-camera' android='md-camera' style={{ fontSize: 27, color: 'white' }} /> 
                            </Button>  
                        </View>
                    </View>
                </View>
                <View style={{ justifyContent: 'flex-end', alignItems: 'center', marginTop: 10 }}> 
                    <View>
                        <Button success onPress={() => { this.props.alteraOpen(false); }} >
                            <Text style={{ color: 'white', fontSize: 14, fontWeight: 'bold', paddingHorizontal: 16 }}> ENVIAR </Text>
                        </Button>  
                    </View >
                </View>

*/
