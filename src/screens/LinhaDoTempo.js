import React, { Component } from 'react';
import { NativeModules, LayoutAnimation, TouchableHighlight, 
  View, Image, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';

import Timeline from '../components/TimeLine';
import Atividades from '../mock/Atividades';
import { modificaAtividade } from '../actions/AtividadeActions';
import { alteraOpen } from '../actions/CriarAtividadeActions';

const { UIManager } = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);

class LinhaDoTempo extends Component {
    constructor(props) {
        super(props);

        console.log(props);

        this.onEventPress = this.onEventPress.bind(this);
        this.onEventFollow = this.onEventFollow.bind(this);
        this.renderSelected = this.renderSelected.bind(this);
        this.renderDetail = this.renderDetail.bind(this);
        this.onEventDropBox = this.onEventDropBox.bind(this);
        this.criarAtividade = this.criarAtividade.bind(this);
  
        this.data = Atividades;

        this.state = { selected: null, follow: null, show: null, modalVisible: 'none' };
      } 
    
      onEventPress(data) {
        this.setState({ selected: data });
      }

      onEventFollow(data) {
        this.setState({ follow: data });
        if (data.follow === true) {
          console.log('estrela ativada');
        } else {
          console.log('estrela desativada');
        }
      }

      onEventDropBox(data) {
        this.setState({ show: data });
      }

      criarAtividade() {
        if (this.state.modalVisible === 'none') {
          this.setState({ modalVisible: 'flex' });
        } else {
          this.setState({ modalVisible: 'none' });
        } 
        LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
      }
    
      defineCor(urgence) {
        switch (urgence) {
          case 'muito_alta': 
            return '#EB5757';
          case 'alta': 
            return '#F2994A';
          case 'media': 
            return '#2F80ED';
          case 'baixa': 
            return '#27AE60';
          case 'muito_baixa': 
            return '#1AB394';
          default: return 'gray';
        }
      }


      renderSelected() {
          if (this.state.selected) {
            return (
                        <Text style={{ marginTop: 10 }}>
                          Selected event: {this.state.selected.title}; Urgence:  {this.state.selected.urgence}
                        </Text>
                    ); 
          }
      }
      

      renderDetail(rowData, sectionID, rowID) {
        let desc = null;

        if (rowData.title) {
            rowData.color = this.defineCor(rowData.urgence);
            rowData.lineColor = rowData.color;
            desc = (
                        <View style={styles.descriptionContainer}>
                          
                          <View style={{ backgroundColor: rowData.color, borderRadius: 15, marginLeft: 20, width: '95%' }}>  
                          
                            <View style={{ margin: 5, padding: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}> 
                            <TouchableOpacity 
                                  onPress={() => {
                                                    if (rowData.show === 'none') {
                                                      console.log(rowData.historicoAtividade);
                                                      rowData.show = 'flex';
                                                      LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);  
                                                    } else {
                                                      rowData.show = 'none';
                                                      LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);  
                                                    }                                               
                                                    this.onEventDropBox(this);
                                                  }
                                  }
                            >
                              <View style={{ width: '88%' }}>
                                <Text style={[styles.title]}>{rowData.title}</Text>
                                <Text style={[styles.textDescription]}>{rowData.description}</Text>
                              </View>
                              </TouchableOpacity>
                              <View style={{ }}> 
                                <TouchableOpacity 
                                  onPress={() => {
                                      if (rowData.follow === false) {
                                        rowData.follow = true;
                                        rowData.followIcon = require('../imgs/star.png');
                                      } else if (rowData.follow === true) {
                                        rowData.follow = false;
                                        rowData.followIcon = require('../imgs/star-o.png');
                                      }
                                      this.onEventFollow(this);
                                  }
                                }
                                >
                                 <Image style={{ width: 25, height: 25 }} source={rowData.followIcon} />
                                </TouchableOpacity>
                              </View>
                            </View>
                            <View>
                              <View style={{ display: rowData.show }}>  
                                <View style={{ alignContent: 'center', width: '100%', backgroundColor: 'white' }} >
                                    <View style={{ height: 'auto', borderLeftColor: '#E0E0E0', borderRightColor: '#E0E0E0', borderRightWidth: 1, borderLeftWidth: 1 }} >
                                        
                                    <TouchableOpacity 
                                            onPress={() => {
                                                              if (rowData.show === 'none') {
                                                                rowData.show = 'flex';
                                                                LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);  
                                                              } else {
                                                                rowData.show = 'none';
                                                                LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);  
                                                              }                                               
                                                              this.onEventDropBox(this);
                                                            }
                                            }
                                    >

                                          <Text style={{ textAlign: 'center', fontSize: 18, width: '100%' }}>
                                            Descrição da ação a ser tomada
                                          </Text>
                                          <Text style={{ textAlign: 'center', fontSize: 18, width: '100%' }}>
                                            Texto falando alguma coisa da ação
                                          </Text>
                                          <Text style={{ textAlign: 'center', fontSize: 18, width: '100%' }}>
                                            Data limite: 08/09/2017
                                          </Text>
                                        </TouchableOpacity>
                                    </View>
                                    
                                    <View style={{ borderBottomLeftRadius: 15, borderBottomRightRadius: 15, width: '100%', backgroundColor: '#E0E0E0', height: 30 }}>
                                      <TouchableHighlight onPress={() => { this.props.modificaAtividade(rowData.title, rowData.description, rowData.historicoAtividade); Actions.historicoAtividade(); }} underlayColor='transparent' >
                                        <Text style={{ textAlign: 'center', fontSize: 18 }}>
                                            Ver histórico
                                        </Text>
                                      </TouchableHighlight>
                                    </View>
                                </View>
                              </View>
                            </View>
                          </View>
                        </View>
                    ); 
        }
        return (
          <View style={{ flex: 1 }}>
            {desc}
          </View>
        );
      }

      
      render() {
        return (
            <View style={styles.container}>
              <Timeline 
                style={styles.list}
                data={this.data}
                lineWidth={2}
                timeContainerStyle={{ display: 'none', backgroundColor: 'transparent' }}
                descriptionStyle={{ color: 'gray' }}
                options={{
                  style: { paddingTop: 5, paddingLeft: 10 }
                }}
                circleColor='transparent'
                innerCircle={'icon'}
                renderDetail={this.renderDetail}
                iconStyle={{}}
              />
              <ActionButton
                icon={<Icon name="md-create" style={styles.actionButtonIcon} />}
                fixNativeFeedbackRadius
                buttonColor="rgba(231,76,60,1)"
                onPress={() => { Actions.criarAtividade(); }}
              />
            </View>
        );
      }
}
       
const mapStateToProps = state => (
  {
      title: state.AtividadeReducer.title,
      description: state.AtividadeReducer.description,
      historicoAtividade: state.AtividadeReducer.historicoAtividade
  }
);

export default connect(mapStateToProps, { modificaAtividade, alteraOpen })(LinhaDoTempo);

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white'
    },
    list: {
      flex: 1,
    },
    title: {
      fontSize: 16,
      color: 'white',
      fontWeight: 'bold'
    },
    descriptionContainer: {
      flexDirection: 'row',
      marginRight: 20
    },
    image: {
      width: 50,
      height: 50,
      borderRadius: 25
    },
    textDescription: {
      marginLeft: 10,
      color: 'white'
    },
    actionButtonIcon: {
      fontSize: 20,
      height: 22,
      color: 'white',
    }
  });
