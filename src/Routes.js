import React, { Component } from 'react';
import { Text, Image } from 'react-native';
import { Router, Scene, Stack, Actions, Drawer } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { Icon as RNIcon, Button, View } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { criarAtividade } from './actions/CriarAtividadeActions';

import SideMenuComponent from './components/SideMenuComponent';

import FormLogin from './screens/FormLogin';
import FormCadastro from './screens/FormCadastro';
import LinhaDoTempo from './screens/LinhaDoTempo';
import HistoricoAtividade from './screens/HistoricoAtividade';
import CriarAtividade from './screens/CriarAtividade';
import Sobre from './screens/Sobre';

class Routes extends Component {
    menu = (props) => (
        <Button transparent onPress={() => { Actions.drawerOpen(); }}>
            <RNIcon ios='ios-menu' android='md-menu' style={{ fontSize: 27, color: 'black' }} />
        </Button>
        )                                                                         
    back = (props) => (
        <Button transparent onPress={() => { Actions.pop(); }}>
            <RNIcon ios='ios-arrow-back-outline' android='md-arrow-back' style={{ fontSize: 27, color: 'black' }} />
        </Button>
    )

    buttons = (props) => (
        <View style={{ flexDirection: 'row' }}>
            <Button transparent onPress={() => { alert('Anexar arquivo'); }} >
                <Icon name='paperclip' size={27} color='black' /> 
            </Button>
            <Button transparent onPress={() => { alert('Abrir camera'); }} >
                <RNIcon ios='ios-camera' android='md-camera' style={{ fontSize: 27, color: 'black' }} /> 
            </Button>
            <Button transparent onPress={() => { alert('Mensagem enviada'); }} >
                <RNIcon ios='ios-paperplane' android='md-send' style={{ fontSize: 27, color: 'black' }} /> 
            </Button>
        </View>
    )
    render() {
        return (
                    <Router>
                        <Scene key='root' hideNavBar >
                            <Scene key='login' >
                                    <Scene key='formLogin' initial='true' component={FormLogin} title="Login" hideNavBar />
                                    <Scene key='formCadastro' component={FormCadastro} title="Esqueci minha senha" />
                            </Scene>
                            <Drawer key="drawer" contentComponent={SideMenuComponent} >
                                <Scene key='menu' >
                                    <Scene key='linhaDoTempo' component={LinhaDoTempo} title="Linha do tempo" hideNavBar={false} left={this.menu} />
                                    <Scene key='historicoAtividade' component={HistoricoAtividade} title="Histórico da Atividade" hideNavBar={false} left={this.back} />    
                                    <Scene key='sobre' component={Sobre} title="Sobre" hideNavBar={false} />
                                    <Scene key='criarAtividade' component={CriarAtividade} title='Nova Solicitação' right={this.buttons} left={this.back} />
                                </Scene>
                            </Drawer>
                        </Scene>
                    </Router>
                );
    }
}

const mapStateToProps = state => (
    {
        itemTipo: state.CriarAtividadeReducer.itemTipo
    }
  );

export default connect(mapStateToProps, { criarAtividade })(Routes);
