const Atividades = [
                        {
                            title: 'Atividades em outro arquivo',
                            description: 'Aguardando envio das folhas de ponto',
                            icon: require('../imgs/exclamation.png'),
                            historicoAtividade: [{
                                id: '01',
                                autor: 'contador',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                                }, {
                                id: '02',
                                autor: 'funcionario',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                                }, {
                                id: '03',
                                autor: 'cliente',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                                }],
                            imageUrl: 'https://cloud.githubusercontent.com/assets/21040043/24240340/c0f96b3a-0fe3-11e7-8964-fe66e4d9be7a.jpg',
                            urgence: 'muito_alta',
                            followIcon: require('../imgs/star-o.png'),
                            follow: false,
                            show: 'none'
                        },
                        {
                            title: 'Play Badminton', 
                            description: 'Badminton is a racquet sport played using racquets to hit a shuttlecock across a net.', 
                            icon: require('../imgs/check.png'),
                            historicoAtividade: [{
                                id: '01',
                                autor: 'contador',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                                }, {
                                id: '02',
                                autor: 'funcionario',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                                }, {
                                id: '03',
                                autor: 'cliente',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                                }, {
                                    id: '04',
                                    autor: 'funcionario',
                                    data: '18/09/2017',
                                    descricao: 'enviar documentos para sefaz'
                                }, {
                                    id: '05',
                                    autor: 'cliente',
                                    data: '18/09/2017',
                                    descricao: 'enviar documentos para sefaz'
                                }, {
                                    id: '06',
                                    autor: 'funcionario',
                                    data: '18/09/2017',
                                    descricao: 'enviar documentos para sefaz'
                                }, {
                                    id: '07',
                                    autor: 'cliente',
                                    data: '18/09/2017',
                                    descricao: 'enviar documentos para sefaz'
                                }, {
                                    id: '08',
                                    autor: 'funcionario',
                                    data: '18/09/2017',
                                    descricao: 'enviar documentos para sefaz'
                                }, {
                                    id: '09',
                                    autor: 'cliente',
                                    data: '18/09/2017',
                                    descricao: 'enviar documentos para sefaz'
                                }, {
                                    id: '10',
                                    autor: 'funcionario',
                                    data: '18/09/2017',
                                    descricao: 'enviar documentos para sefaz'
                                }, {
                                    id: '11',
                                    autor: 'cliente',
                                    data: '18/09/2017',
                                    descricao: 'enviar documentos para sefaz'
                                }],
                            imageUrl: 'https://cloud.githubusercontent.com/assets/21040043/24240405/0ba41234-0fe4-11e7-919b-c3f88ced349c.jpg',
                            urgence: 'baixa',
                            followIcon: require('../imgs/star-o.png'),
                            follow: false,
                            show: 'none'
                        },
                        {
                            title: 'Lunch', 
                            description: 'Look out for the Best Gym & Fitness Centers around me :)', 
                            icon: require('../imgs/hourglass-half.png'),
                            historicoAtividade: [{
                                id: '01',
                                autor: 'contador',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                            }, {
                                id: '02',
                                autor: 'funcionario',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                            }, {
                                id: '03',
                                autor: 'cliente',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                            }, {
                                id: '04',
                                autor: 'funcionario',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                            }, {
                                id: '05',
                                autor: 'cliente',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                            }, {
                                id: '06',
                                autor: 'funcionario',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                            }, {
                                id: '07',
                                autor: 'cliente',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                            }],
                            urgence: 'media',
                            followIcon: require('../imgs/star-o.png'),
                            follow: false,
                            show: 'none'
                        },
                        {
                            title: 'Watch Soccer', 
                            description: 'Team sport played between two teams of eleven players with a spherical ball. ',
                            icon: require('../imgs/question.png'),
                            historicoAtividade: [{
                                id: '01',
                                autor: 'contador',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                            }, {
                                id: '02',
                                autor: 'funcionario',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                            }, {
                                id: '03',
                                autor: 'cliente',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                            }, {
                                id: '04',
                                autor: 'funcionario',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                            }, {
                                id: '05',
                                autor: 'cliente',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                            }, {
                                id: '06',
                                autor: 'cliente',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                            }, {
                                id: '07',
                                autor: 'funcionario',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                            }, {
                                id: '08',
                                autor: 'cliente',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                            }],
                            imageUrl: 'https://cloud.githubusercontent.com/assets/21040043/24240419/1f553dee-0fe4-11e7-8638-6025682232b1.jpg',
                            urgence: 'alta',
                            followIcon: require('../imgs/star-o.png'),
                            follow: false,
                            show: 'none'
                        },
                        {
                            title: 'Teste de Time line', 
                            description: 'Descrição do teste time line', 
                            icon: require('../imgs/exclamation.png'),
                            historicoAtividade: [{
                                id: '01',
                                autor: 'contador',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                                }, {
                                id: '02',
                                autor: 'funcionario',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                                }, {
                                id: '03',
                                autor: 'cliente',
                                data: '18/09/2017',
                                descricao: 'enviar documentos para sefaz'
                                }, {
                                    id: '04',
                                    autor: 'funcionario',
                                    data: '18/09/2017',
                                    descricao: 'enviar documentos para sefaz'
                                }, {
                                    id: '05',
                                    autor: 'cliente',
                                    data: '18/09/2017',
                                    descricao: 'enviar documentos para sefaz'
                                }, {
                                    id: '06',
                                    autor: 'funcionario',
                                    data: '18/09/2017',
                                    descricao: 'enviar documentos para sefaz'
                                }, {
                                    id: '07',
                                    autor: 'cliente',
                                    data: '18/09/2017',
                                    descricao: 'enviar documentos para sefaz'
                                }, {
                                    id: '08',
                                    autor: 'funcionario',
                                    data: '18/09/2017',
                                    descricao: 'enviar documentos para sefaz'
                                }, {
                                    id: '09',
                                    autor: 'cliente',
                                    data: '18/09/2017',
                                    descricao: 'enviar documentos para sefaz'
                                }, {
                                    id: '10',
                                    autor: 'funcionario',
                                    data: '18/09/2017',
                                    descricao: 'enviar documentos para sefaz'
                                }, {
                                    id: '11',
                                    autor: 'cliente',
                                    data: '18/09/2017',
                                    descricao: 'enviar documentos para sefaz'
                                }, {
                                    id: '12',
                                    autor: 'cliente',
                                    data: '18/09/2017',
                                    descricao: 'enviar documentos para sefaz'
                                }, {
                                    id: '13',
                                    autor: 'funcionario',
                                    data: '18/09/2017',
                                    descricao: 'enviar documentos para sefaz'
                                }, {
                                    id: '14',
                                    autor: 'cliente',
                                    data: '18/09/2017',
                                    descricao: 'enviar documentos para sefaz'
                                }],
                            imageUrl: 'https://cloud.githubusercontent.com/assets/21040043/24240422/20d84f6c-0fe4-11e7-8f1d-9dbc594d0cfa.jpg',
                            urgence: 'muito_alta',
                            followIcon: require('../imgs/star-o.png'),
                            follow: false,
                            show: 'none'
                        }
                    ];

export default Atividades;
