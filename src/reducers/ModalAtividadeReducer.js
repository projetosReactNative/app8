const INITIAL_STATE = {
    open: false,
    items: [{
        id: 0,
        label: 'Abertura de filial',
        value: 'abertura_filia'      
      }, {
        id: 1,
        label: 'Parcelamento de debitos',
        value: 'parcelamento_debitos'      
      }, {
        id: 2,
        label: 'Declaração de imposto',
        value: 'declaracao_imposto'      
      }, {
        id: 3,
        label: 'Resumo de notas',
        value: 'resumo_notas'      
      }, {
        id: 4,
        label: 'Folhas de pagamento',
        value: 'folhas_pagamento'      
      }]
};

export default (state = INITIAL_STATE, action) => {
    if (action.type === 'altera_open') {
        console.log(state.open);
        return { ...state, open: action.open };
    }
    return (state);
};
