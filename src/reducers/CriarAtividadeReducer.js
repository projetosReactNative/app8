const INITIAL_STATE = {
    anexoAtividade: null,
    descricaoAtividade: null,
    tipoAtividade: null,
    items: [{
        id: 0,
        label: 'Abertura de filial',
        value: 'abertura_filia'      
      }, {
        id: 1,
        label: 'Parcelamento de debitos',
        value: 'parcelamento_debitos'      
      }, {
        id: 2,
        label: 'Declaração de imposto',
        value: 'declaracao_imposto'      
      }, {
        id: 3,
        label: 'Resumo de notas',
        value: 'resumo_notas'      
      }, {
        id: 4,
        label: 'Folhas de pagamento',
        value: 'folhas_pagamento'      
      }]
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'altera_open':
      return { ...state, open: action.open };
    case 'tipo_atividade':
      console.log(action.payload);
      return { ...state, tipoAtividade: action.payload };
    case 'descricao_atividade':
      console.log(action.payload);
      return { ...state, descricaoAtividade: action.payload };
    case 'criar_atividade':
      if(state.descricaoAtividade != null & state.tipoAtividade != null){
        alert("Mensagem enviada");
      } else{
        alert("preencha os campos corretamente");
      }
      return (state);
    default:
      return (state);
  }
};
