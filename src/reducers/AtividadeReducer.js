const INITIAL_STATE = {
        title: 'Teste Titulo Atividade', 
        description: 'Teste Descricao Atividade Teste Descricao Atividade Teste Descricao Atividade',
        historicoAtividade: [{
            id: '01',
            autor: 'contador',
            data: '18/09/2017',
            descricao: 'enviar documentos para sefazenviar documentos para sefazenviar documentos para sefazenviar documentos para sefaz'
            }, {
            id: '02',
            autor: 'funcionario',
            data: '18/09/2017',
            descricao: 'enviar documentos para sefazenviar documentos para sefazenviar documentos para sefazenviar documentos para sefaz'
            }, {
            id: '03',
            autor: 'cliente',
            data: '18/09/2017',
            descricao: 'enviar documentos para sefazenviar documentos para sefazenviar documentos para sefaz'
            }, {
                id: '04',
                autor: 'funcionario',
                data: '18/09/2017',
                descricao: 'enviar documentos para sefazenviar documentos para sefazenviar documentos para sefaz'
            }, {
                id: '05',
                autor: 'cliente',
                data: '18/09/2017',
                descricao: 'enviar documentos para sefazenviar documentos para sefaz'
            }],
    };

export default (state = INITIAL_STATE, action) => {
    if (action.type === 'modifica_atividade') {
        return { ...state, title: action.title, description: action.description, historicoAtividade: action.historicoAtividade };
    }
    return (state);
};
