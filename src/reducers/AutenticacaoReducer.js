const INITIAL_STATE = {
    nome: '',
    email: 'pedro@',
    senha: '123',
    confirmarSenha: '',
    erroEmail: '',
    buttonDisabled: false,
    };

export default (state = INITIAL_STATE, action) => {
    if (action.type === 'modifica_email') {
        return { ...state, email: action.payload };
    }
    if (action.type === 'modifica_senha') {
        return { ...state, senha: action.payload };
    }
    if (action.type === 'modifica_nome') {
        return { ...state, nome: action.payload };
    }
    if (action.type === 'modifica_confirmar_senha') {
        return { ...state, confirmarSenha: action.payload };
    }
    if (action.type === 'valida_email') {
        const email = action.payload.email;
        const senha = action.payload.senha;

        let erroEmail = '';
        let buttonDisabled = true;
        
        if (email.indexOf('@') === -1) {
             erroEmail = 'Digite um email válido!';
        } else if (senha.length > 0) {
                erroEmail = buttonDisabled;
                buttonDisabled = false;
            }

        return { ...state, erroEmail, buttonDisabled };
    }

    return (state);
};
