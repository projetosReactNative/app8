import { combineReducers } from 'redux';
import AutenticacaoReducer from './AutenticacaoReducer';
import AtividadeReducer from './AtividadeReducer'; 
import CriarAtividadeReducer from './CriarAtividadeReducer'; 

export default combineReducers({
    AutenticacaoReducer,
    AtividadeReducer,
    CriarAtividadeReducer
});
