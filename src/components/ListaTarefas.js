import React, { Component } from 'react';
import {
  Text, View, StyleSheet
} from 'react-native';
import { color1 } from './Colors';

export default class ListaTarefas extends Component {
  defineBackground(autor) {
    const styles = {
        backgroundColor: '#2D9CDB',
    };

    if (autor === 'cliente') {
        styles.backgroundColor = '#27AE60'; 
    } 
    return styles;
  }

  definePosicao(autor) {
    const styles = {
      alignItems: 'flex-start',
    };

    if (autor === 'cliente') {
        styles.alignItems = 'flex-end';
    } 
    return styles;
}

  render() {
    return (
      <View style={this.definePosicao(this.props.item.autor)}>
        <View style={[styles.item, this.defineBackground(this.props.item.autor)]}>
          <View style={{ padding: 8 }}>
            <Text style={styles.autor}>{this.props.item.autor}</Text>
            <Text style={styles.descricao}>{this.props.item.descricao}</Text>
            <Text style={styles.data}>{this.props.item.data}</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  item: {
    borderRadius: 5,
    width: '75%',
    margin: 10
  },
  details: {
    flex: 1
  },
  autor: {
    color: color1,
    fontWeight: '200',
    paddingBottom: 2
  },
  data: {
    fontSize: 10,
    textAlign: 'right',
    color: color1,
    fontWeight: '200',
  },
  descricao: {
    fontSize: 15,
    color: 'white',
  }
});
