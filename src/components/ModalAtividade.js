import React, { Component } from 'react';
import Modal from 'react-native-simple-modal';
import { ScrollView, Text, View, Platform, Picker, TextInput, Button as Btn, Keyboard, TouchableWithoutFeedback } from 'react-native';
import { connect } from 'react-redux';
import { Icon as RNIcon, Button } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { alteraOpen } from '../actions/ModalAtividadeActions';
import ModalPicker from './ModalPicker';

 
class ModalAtividade extends Component {
 constructor(props) {
     super(props);
     this.state = { offset: 0, open: true, value: 'selecione', display: 'none', color: 'gray', textInputHeight: 40 };
     this.tipos = this.props.items;

     if(Platform.OS === 'ios'){
         this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this))
         this.keyboardDidHideListiner = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this))
     }
 }

 defineModalPicker() {
     if (Platform.OS === 'ios') {
         return (<ModalPicker />);
     } 
        return (
            <View>
                <Text style={{ textAlign: 'center', display: this.state.display }}>
                    Selecione o tipo
                </Text>
                <Picker style={{ color: this.state.color }} onValueChange={(itemValue) => { this.setState({ value: itemValue, display: 'none', color: 'black' }); }} selectedValue={this.state.value}>
                    <Picker enabled={false} label="Selecione o tipo" value='selecione' style={{color: 'blue'}} />
                    {this.tipos.map((item) => (<Picker.Item disabled selected key={item.id} label={item.label} value={item.value} />))}
                </Picker>
            </View>
        );
 }

 heightChange(event) {
    const height = event.nativeEvent.contentSize.height;
    if (height > 40) {
        this.setState({ textInputHeight: height });
    } else{
        this.setState({ textInputHeight: 40})
    }
 }

 _keyboardDidShow() {
    this.setState({offset: -100})
 }

 _keyboardDidHide() {
    this.setState({offset: 0})
 }

  render() {
    return (
        <Modal
            transparent
            offset={this.state.offset}
            open={this.props.open}
            modalDidClose={() => { this.props.alteraOpen(false); }}
            >
        
            
            <TouchableWithoutFeedback onPress={ Keyboard.dismiss }>
                <View style={{ backgroundColor: '#1AB394', margin: -10, paddingHorizontal: 10}}>
                
                    <View style={{ flexDirection: 'row', paddingVertical: 8 }}>
                        <View style={{ flex: 6, alignItems: 'flex-start', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 20, fontWeight: '800', color: 'white', paddingLeft: 12, paddingTop: 10 }} > Nova Solicitação </Text>
                        </View>
                    </View>
                    <View style={{ padding: 8, paddingHorizontal: 14}}>
                        <View style={{ backgroundColor: 'rgba(242,242,242,0.8)', borderRadius: 4, marginTop: 5, justifyContent: 'center', height: Platform.OS === 'ios' ? 40 : undefined }}>
                            {this.defineModalPicker()}
                        </View>
                        <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 10, backgroundColor: 'rgba(242,242,242,0.8)', borderRadius: 4 }}> 
                            <TextInput  multiline returnKeyType='done' placeholder="Descrição da atividade" style={{ width: '100%', height: this.state.textInputHeight, fontSize: 16, paddingHorizontal: 14, paddingTop: 10 }} onContentSizeChange={this.heightChange.bind(this)} underlineColorAndroid='transparent' />
                        </View>
                        <View style={{ backgroundColor: 'rgba(242,242,242,0.8)', borderRadius: 4, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', marginTop: 10 }}> 
                            <View style={{ flex: 8, justifyContent: 'center', paddingLeft: 8 }}>
                                <Text> Anexo </Text>
                            </View>
                            <View >
                                <View >
                                    <Button transparent onPress={() => { alert('selecionar arquivo'); }} >
                                        <Icon name='paperclip' size={27} color='black' /> 
                                    </Button>  
                                </View>
                            </View>
                            <View style={{ marginHorizontal: 5 }}>
                                <View >
                                    <Button transparent onPress={() => { alert('usar camera'); }} >
                                        <RNIcon ios='ios-camera' android='md-camera' style={{ fontSize: 27, color: 'black' }} /> 
                                    </Button>  
                                </View>
                            </View>
                        </View>
                        <View style={{ justifyContent: 'flex-end', alignItems: 'center', marginTop: 10, flexDirection: 'row' }}> 
                            <View style={{ marginRight: 8 }}>
                                <Button onPress={() => { this.props.alteraOpen(false); }} >
                                    <Text style={{ color: 'white', fontSize: 14, fontWeight: 'bold' }}> CANCELAR </Text>
                                </Button>  
                            </View>
                            <View>
                                <Button onPress={() => { this.props.alteraOpen(false); }} >
                                    <Text style={{ color: 'white', fontSize: 14, fontWeight: 'bold' }}> ENVIAR </Text>
                                </Button>  
                            </View >
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback> 
        </Modal>
        
    );
  }
}

const mapStateToProps = state => (
    {
        open: state.ModalAtividadeReducer.open,
        items: state.ModalAtividadeReducer.items
    }
);

export default connect(mapStateToProps, { alteraOpen })(ModalAtividade);
