import React, { Component } from 'react';
import { View, Header, Title, Button, Icon, Right, 
  Body, Left, Picker, Item as FormItem } from 'native-base';
import { connect } from 'react-redux';

  const Item = Picker.Item;

class ModalPicker extends Component {
  constructor(props) {
    super(props);
    this.tipos = this.props.items;
    this.state = {
      selected2: undefined
    };
  }
  onValueChange2(value) {
    this.setState({
      selected2: value
    });
  }
  render() {
    return (
            <Picker
              renderHeader={backAction =>
                <Header >
                  <Left>
                    <Button transparent onPress={backAction}>
                      <Icon name="close" style={{ color: '#000' }} />
                    </Button>
                  </Left>
                  <Body style={{ flex: 3 }}>
                    <Title style={{ color: '#000' }}>Selecione um tipo</Title>
                  </Body>
                  <Right />
                </Header>}
              style={{ height: '100%', width: '100%', justifyContent: 'center' }}
              headerBackButtonText='Cancelar'
              mode="dropdown"
              placeholder="Selecione um tipo"
              selectedValue={this.state.selected2}
              onValueChange={this.onValueChange2.bind(this)}
            >
              {this.tipos.map((item) => (<Item key={item.id} label={item.label} value={item.id} />))}
            </Picker>
    );
  }
}

const mapStateToProps = state => (
  {
      open: state.ModalAtividadeReducer.open,
      items: state.ModalAtividadeReducer.items
  }
);

export default connect(mapStateToProps, null)(ModalPicker);
