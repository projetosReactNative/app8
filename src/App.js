import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import firebase from 'firebase';

import Routes from './Routes';
import reducers from './reducers';

class App extends Component {
    componentWillMount() {
        // Initialize Firebase
        const config = {
            apiKey: 'AIzaSyBJG8n6AA0lWRPFtCFiO8X7m4SlrHw_O4s',
            authDomain: 'app8-e1fdf.firebaseapp.com',
            databaseURL: 'https://app8-e1fdf.firebaseio.com',
            projectId: 'app8-e1fdf',
            storageBucket: 'app8-e1fdf.appspot.com',
            messagingSenderId: '206004030331'
        };
        firebase.initializeApp(config);
    }

    render() {
        return (
            <Provider store={createStore(reducers)}>
                <Routes />
            </Provider>
        );
    }
}

export default App;
