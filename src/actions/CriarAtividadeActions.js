export const alteraOpen = (open) => ({
    type: 'altera_open',
    open
});

export const criarAtividade = () => ({
    type: 'criar_atividade',
});

export const alteraTipoAtividade = (payload) => ({
    type: 'tipo_atividade',
    payload
});

export const alteraDescricaoAtividade = (payload) => ({
    type: 'descricao_atividade',
    payload
});

