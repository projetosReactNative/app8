export const modificaAtividade = (title, description, historicoAtividade) => ({
        type: 'modifica_atividade',
        title,
        description,
        historicoAtividade
    });
