export const modificaEmail = (texto) => ({
        type: 'modifica_email',
        payload: texto
});

export const modificaSenha = (texto) => ({
    type: 'modifica_senha',
    payload: texto
});

export const modificaNome = (texto) => ({
    type: 'modifica_nome',
    payload: texto
});

export const modificaConfirmarSenha = (texto) => ({
    type: 'modifica_confirmar_senha',
    payload: texto
});

export const validaEmail = (email, senha) => ({
    type: 'valida_email',
    payload: { email, senha }
});

export const cadastraUsuarios = ({ nome, email, senha }) => {
    alert(`${nome}\n${email}\n${senha}\n`);
    return {
        type: 'teste'
    };
};
